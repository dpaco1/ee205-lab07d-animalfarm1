/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 10_Mar_2022
////////////////////////////////////
#pragma once

int updateCatName(int index, char newName[]);

int fixCat(int index);

int updateCatWeight(int index, float newWeight);

int updateCatCollar1(int index, int newCollar1);

int updateCatCollar2(int index, int newCollar2);

int updateLicense(int index, int newLicense);
