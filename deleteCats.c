/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 10_Mar_2022
////////////////////////////////////
#include <stdio.h>

#include "catDatabase.h"
#include "deleteCats.h"
#include "config.h"

int deleteAllCats(void){
   CURRENT_CATS = 0;
   return 1;
}
