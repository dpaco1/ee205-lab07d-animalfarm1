/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 10_Mar_2022
////////////////////////////////////
#pragma once

extern int printCat(int index);

extern int printAllCats(void);

extern int findCat(char name[]);

